import { Component, OnInit, Inject, LOCALE_ID, Input } from '@angular/core';
import { TimeDayService } from '../services/timeday/time-day.service';
import { TimeDay } from '../services/timeday/TimeDay';
import { CalendarOptions, EventInput } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin from '@fullcalendar/interaction';
import Swal from 'sweetalert2';
import { formatDate } from '@angular/common';
import { Project } from '../services/project/Project';
import { ProjectService } from '../services/project/project.service';
import { Router } from '@angular/router';
import { GlobalVar } from '../global-variables';
import { jsPDF, TableConfig } from 'jspdf'
import { IconDefinition, faFilePdf } from '@fortawesome/free-solid-svg-icons';
import { UsersService } from '../services/users.service';
import { User } from '../services/User';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-time-day',
  templateUrl: './time-day.component.html',
  styleUrls: ['./time-day.component.css']
})
export class TimeDayComponent implements OnInit {

  @Input() targetUser: User |null=null;
  @Input() project: Project |undefined;
  allProjects: any = {};
  events: any[] = [];
  faFilePdf: IconDefinition = faFilePdf;

  calendarOptions: CalendarOptions = {
    initialView: 'dayGridMonth',
    plugins: [interactionPlugin, dayGridPlugin],
    events: this.events,
    selectable: true,
    dragScroll: true,
    height: 'auto',
    firstDay: 1,
    businessHours: true,
    dayMaxEventRows: true,
    eventClick: this.eventClick.bind(this),
    select: this.handleSelectClick.bind(this),
    selectAllow: this.allowSelect.bind(this),
  };
  eventsPromise: Promise<EventInput> | undefined;

  constructor(private timeDayService: TimeDayService, private projectService: ProjectService, private userService: UsersService, @Inject(LOCALE_ID) public locale: string, private _router: Router) {
    
  }

  ngOnInit(): void {
    if(!GlobalVar.connectedUser){
      this._router.navigate(['login'])
    }
    this.loadTimeDays();
    this.projectService.load().subscribe(ps => ps.forEach((p: Project) => this.allProjects[p.id] = p.name));
  }


  loadTimeDays(): void {
    let USER = GlobalVar.connectedUser;
    if(this.targetUser){
      USER = this.targetUser;
    }
    console.log(this.project)
    console.log(this.targetUser)
    this.events = [];
    if (this.project) {
      this.timeDayService.loadByProjectId(this.project?.id!).subscribe(timedays => {
        timedays.forEach((timeday: TimeDay) => {
          this.userService.loadById(timeday.user.id).subscribe((user: User) => {
            this.events.push({ title: "[" + user.firstname + " " + user.lastname + "] " + (timeday.granularity * 100).toString() + "%", date: timeday.date, id: timeday.id })
          })
        })
      });
    } else {
      this.timeDayService.loadByUserId(USER?.id!).subscribe(timedays => {
        timedays.forEach((timeday: TimeDay) => {
          this.events.push({ title: "[" + timeday.project.name + "] " + (timeday.granularity * 100).toString() + "%", date: timeday.date, id: timeday.id });
        })
      });
    }
    this.calendarOptions.events = this.events;
  }

  allowSelect(arg: any): boolean {
    const Sat = 6;
    const Sun = 0;
    if (arg.start.getDay() == Sat || arg.start.getDay() == Sun) {
      return false
    } else return true
  }

  eventClick(arg: any) {
    Swal.fire({
      title: "Voulez vous supprimer ce créneau ?",
      text: arg.event.startStr + "=>" + arg.event.title,
      icon: "question",
      confirmButtonText: "Oui, supprimer !",
      confirmButtonColor: 'red',
      showCancelButton: true,
      cancelButtonText: "Non",
    }).then((result) => {
      if (result.isConfirmed) {
        this.timeDayService.delete(arg.event.id)
        Swal.showLoading();
        Swal.fire("Créneau supprimé", "le créneau a été supprimé !", "success").then((result) => { if (result.isConfirmed) this.loadTimeDays(); });
      }
    });
  }

  async handleSelectClick(arg: any) {
    let USER = GlobalVar.connectedUser;
    if(this.targetUser){
      USER = this.targetUser;
    }
    let isCancelled = false;

    const Sat = 6;
    const Sun = 0;
    let nb_jour: number = (arg.end - arg.start) / 24 / 3600 / 1000;
    let containWeekend: boolean = false;
    let arrayDate: string[] = [];
    let date: Date = new Date(arg.startStr);
    for (let i: number = 0; i < nb_jour; i++) {
      date.setDate(arg.start.getDate() + i);
      if (date.getDay() == Sat || date.getDay() == Sun) containWeekend = true;
      else arrayDate.push(formatDate(date, "yyyy-MM-dd", this.locale));
    }

    if (arrayDate.length > 1) {
      await Swal.fire({
        title: "Vous avez sélectionné plusieurs jours!",
        text: "Sur chaque jour sélectionné, un créneau sera créé avec le même projet et la même granularité.",
        icon: "warning",
        confirmButtonText: "Oui, continuer",
        confirmButtonColor: 'green',
        showCancelButton: true,
        cancelButtonText: "Annuler",
      }).then(result => {
        if (!result.isConfirmed) isCancelled = true;
      });
    }

    if (containWeekend && !isCancelled) await Swal.fire("La sélection contient des weekend !", "La sélection contient au moins un Samedi ou un Dimanche. Les créneaux sur ces weekend ne seront pas ajoutés !", "warning");

    var targetProject: Project = { id: 0, name: "" };
    var granularity: number = 0;
    if (!isCancelled) {
      if (this.project) targetProject = this.project;
      else {
        await Swal.fire({
          title: "Sélectionner le projet",
          text: "Sélectionne un projet parmi la liste :",
          icon: "info",
          input: "select",
          inputPlaceholder: 'Select a project',
          inputOptions: this.allProjects,
          confirmButtonText: "Ok",
          showCancelButton: true,
          cancelButtonText: "Annuler",
        }).then(result => {
          if (result.isConfirmed) targetProject = { id: result.value, name: this.allProjects[result.value] };
          else isCancelled = true;
        });
      }
    }

    if (!isCancelled) {
      if (arrayDate.length > 1) var title: string = "Choisissez la granularité pour le projet [" + targetProject.name + "] pour les " + arrayDate.length + " jours !";
      else var title: string = "Choisissez la granularité pour le projet [" + targetProject.name + "] pour le  " + arrayDate[0] + " !";
      await Swal.fire({
        title: title,
        text: "La granularité doit être entre 0 et 100% du jour, et représente la part de travail sur ce projet sur une journée.",
        input: "range",
        icon: "info",
        inputAttributes: { min: "0", max: "100", step: "1" },
        inputValue: 25,
        confirmButtonText: "Ok",
        showCancelButton: true,
        cancelButtonText: "Annuler",
      }).then((result) => {
        if (result.isConfirmed) granularity = result.value;
        else isCancelled = true;
      });
    }

    if (!isCancelled) {
      if (granularity >= 0 && granularity <= 100) {
        if (arrayDate.length > 1) var text: string = arrayDate.length + " days => [" + targetProject.name + "] " + granularity + "%";
        else var text: string = arrayDate[0] + " => [" + targetProject.name + "] " + granularity + "%";
        await Swal.fire({
          title: "Voulez vous créer ce créneau ?",
          text: text,
          icon: "question",
          showCancelButton: true,
          confirmButtonText: "Oui",
          cancelButtonText: "Non"
        }).then(async (result) => {
          if (result.isConfirmed) {
            for (let i: number = 0; i < arrayDate.length; i++) this.timeDayService.post(arrayDate[i], granularity / 100.0, USER?.id!, targetProject.id);
            Swal.showLoading();
            if (arrayDate.length > 1) await Swal.fire("Créneaux ajoutés !", arrayDate.length.toString() + " créneaux ont été ajouté", "success");
            else await Swal.fire("Créneau ajouté !", "Le créneau a été ajouté", "success");
            this.loadTimeDays();
          }
        });
      } else Swal.fire("ERREUR !", "La granularité doit être entre 0 et 100% !", "error");
    }
  }

  async exportPDF() {
    let USER = GlobalVar.connectedUser;
    if(this.targetUser){
      USER = this.targetUser;
    }
    const MONTHS = {
      "2023": { "2023-02": "Février 2023", "2023-01": "Janvier 2023", },
      "2022": { "2022-12": "Décembre 2022", "2022-11": "Novembre 2022", "2022-10": "Octobre 2022", "2022-09": "Septembre 2022", "2022-08": "Aout 2022", "2022-07": "Juillet 2022", "2022-06": "Juin 2022", "2022-05": "Mai 2022", "2022-04": "Avril 2022", "2022-03": "Mars 2022", "2022-02": "Février 2022", "2022-01": "Janvier 2022", },
    }
    let monthChoosen = '2023-01';
    let isCancelled = false;
    console.log("export");

    await Swal.fire({
      title: "Exporter un mois en PDF",
      text: "Sélectionner un moi :",
      icon: "question",
      input: "select",
      inputPlaceholder: "Selectionner un mois",
      inputOptions: MONTHS,
      confirmButtonText: "OK",
      showCancelButton: true,
      cancelButtonText: "Annuler"
    }).then(result => {
      if (result.isConfirmed) monthChoosen = result.value;
      else isCancelled = true;
    });

    if (!isCancelled) {
      let data2export: any[] = [];
      for (let i: number = 0; i < this.events.length; i++) {
        const event = this.events[i];
        if (event.date.indexOf(monthChoosen) != -1) {
          let tab: string[] = event.title.split(']');
          let name: string = tab[0].replace('[', '')
          let granularity: string = tab[1].trim();
          let date: string = event.date;
          data2export.push({ "Nom": name, "Date": date, "Granularite": granularity });
        }
      }
      data2export.sort((a, b) => a.Date.localeCompare(b.Date));

      const doc = new jsPDF();
      //console.log(doc.getFontList());

      doc.setTextColor('#1262b2');
      doc.setFont("Times", "Bold");
      doc.setFontSize(40);
      doc.text("Export des crénaux", 10, 20);

      doc.addImage("../assets/logo_telecom.jpg", "jpg", 130, 10, 80, 29);

      doc.setTextColor('#000000');
      doc.setFont("Helvetica");
      doc.setFontSize(20);

      if (this.project) doc.text("Projet : " + this.project.name, 10, 40);
      else doc.text("Utilisateur : " + USER?.firstname + " " + USER?.lastname, 10, 40);
      doc.text("Mois : " + monthChoosen, 10, 50);

      doc.line(10, 60, 200, 60);

      doc.setFontSize(16);
      const config: TableConfig = { printHeaders: true, autoSize: true, margins: 10 };
      doc.table(10, 70, data2export, ["Nom", "Date", "Granularite"], config);

      if (this.project) doc.save("export_" + monthChoosen + "_" + this.project.name.replace(' ', '_') + ".pdf");
      else doc.save("export_" + monthChoosen + "_" + USER?.lastname + "_" + USER?.firstname + ".pdf");
    }
  }
}
