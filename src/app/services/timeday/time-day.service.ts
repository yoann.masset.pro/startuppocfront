import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { options } from '@fullcalendar/core/preact';
import { URL } from 'url';
import { TimeDay } from './TimeDay';

@Injectable({
  providedIn: 'root'
})
export class TimeDayService {

  api: string = "http://localhost:8081/timeday"

  constructor(private http: HttpClient) { }

  loadAll() {
    return this.http.get<TimeDay[]>(this.api);
  }

  loadById(id: number) {
    return this.http.get<TimeDay[]>(this.api + "/" + id);
  }

  loadByUserId(id: number) {
    return this.http.get<TimeDay[]>(this.api + "/user/" + id);
  }

  loadByUserIdAndProjectId(userId: number, projectId: number) {
    return this.http.get<TimeDay[]>(this.api + "/user/" + userId + "/project/" + projectId);
  }

  loadByProjectId(id: number) {
    return this.http.get<TimeDay[]>(this.api + "/project/" + id);
  }

  post(date: string, granularity: number, userId: number, projectId: number) {
    const headers = { 'content-type': 'application/json' };
    const timeDay = {
      date: date,
      granularity: granularity,
      user: { id: userId },
      project: { id: projectId },
    };
    const body = JSON.stringify(timeDay);
    console.log(body);
    this.http.post(this.api, body, { 'headers': headers }).subscribe(data => { console.log(data); return data });
  }

  delete(id: number) {
    this.http.delete(this.api + "/" + id).subscribe();
  }
}
