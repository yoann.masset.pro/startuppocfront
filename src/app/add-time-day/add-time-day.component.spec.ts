import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTimeDayComponent } from './add-time-day.component';

describe('AddTimeDayComponent', () => {
  let component: AddTimeDayComponent;
  let fixture: ComponentFixture<AddTimeDayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddTimeDayComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddTimeDayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
