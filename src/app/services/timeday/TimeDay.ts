import { Project } from "../project/Project";
import { User } from "../User";

export interface TimeDay {
    id: number;
    date: string;
    granularity: number;
    user: User;
    project: Project;
}