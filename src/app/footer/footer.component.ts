import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  members: string[] = ["Joé TEIXEIRA", "Yoann MASSET", "Teddy SABATIER"];
  date: string = "November 2022";

  constructor() { }

  ngOnInit(): void {
  }

}
