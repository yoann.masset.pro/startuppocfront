import { Component, OnInit, Inject, LOCALE_ID } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TimeDayService } from '../services/timeday/time-day.service';
import { Observable } from 'rxjs';
import { TimeDay } from '../services/timeday/TimeDay';
import { CalendarOptions, EventInput } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin, { Draggable } from '@fullcalendar/interaction';
import Swal from 'sweetalert2';
import { formatDate } from '@angular/common';
import { GlobalVar } from '../global-variables';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-time-day',
  templateUrl: './add-time-day.component.html',
  styleUrls: ['./add-time-day.component.css']
})

export class AddTimeDayComponent implements OnInit {

  project = { id: this.data.project.id, name: this.data.project.name };

  constructor(public dialogRef: MatDialogRef<AddTimeDayComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
  }

  onCancel(): void {
    this.dialogRef.close();
  }
}
