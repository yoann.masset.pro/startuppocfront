import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User, UserType } from './User';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  apiUrl: string = 'http://localhost:8081/users';
  devUrl: string = 'http://localhost:8081/getdevelopers';
  apiDeleteurl: string = 'http://localhost:8081/user';
  apiSwitchRoleUrl:string = 'http://localhost:8081/switchrole';
  apiAddDevUrl:string = 'http://localhost:8081/adddeveloper';
  apiSwitchManagerUrl:string = ('http://localhost:8081/changemanager')

  users: BehaviorSubject<User[]> = new BehaviorSubject<User[]>([])

  constructor(private http: HttpClient) { }

  loadUsers() {
    console.log('loading users')
    return this.http.get<User[]>(this.apiUrl);
  }

  loadById(id: number) {
    return this.http.get<User>(this.apiUrl + "/" + id);//.subscribe((user:User) => {return user;});
  }

  addDeveloper(managerId: number, developer: User) {
    console.log('adding developper ', developer.login, ' to manager ', managerId);
    const headers = { 'content-type': 'application/json' }
    const body = JSON.stringify(developer);
    return this.http.post<any>(this.apiAddDevUrl + '/' + managerId, body, { 'headers': headers });
  }

  loadDevelopers(managerId: number) {
    console.log('loading users managed by ' + managerId)
    return this.http.get<User[]>(this.devUrl + '/' + managerId);
  }

  deleteData(id: number) {
    console.log('deleting user ' + id)
    return this.http.delete(this.apiDeleteurl + '/' + id)
  }
  
  switchManager(developer:User,oldmanager:User, newmanager:User){
    console.log('switching developper ', developer.login, ' to manager ', newmanager.login);
    const headers = { 'content-type': 'application/json'}
    const body=JSON.stringify(developer); 
    return this.http.post<any>(this.apiSwitchManagerUrl+'/'+oldmanager.id+'/'+newmanager.id, body,{'headers':headers});
  }

  switchRole(developer:User,role:UserType){
    console.log('switching developper ', developer.login, ' to role ', role.name);
    const headers = { 'content-type': 'application/json' }
    const body = JSON.stringify(role);
    return this.http.patch<any>(this.apiSwitchRoleUrl + '/' + developer.id, body, { 'headers': headers });
  }

}
