import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoginForm } from './LoginForm';
import { GlobalVar } from '../global-variables'

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  apiUrl: string = 'http://localhost:8081/login'

  constructor(private http: HttpClient) {
  }

  LogIn(loginForm: LoginForm) {
    console.log('trying login with', loginForm.login)
    const headers = { 'content-type': 'application/json' }
    const body = JSON.stringify(loginForm);
    return this.http.post<any>(this.apiUrl, body, { 'headers': headers })
  }
}
