import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { User } from '../services/User';

@Component({
  selector: 'app-role-panel',
  templateUrl: './role-panel.component.html',
  styleUrls: ['./role-panel.component.css']
})
export class RolePanelComponent implements OnInit {

  @Input() user:User|null=null;

  @Output() childEvent = new EventEmitter();
  switchRole(){
    this.childEvent.emit(this.selectedrole);
  }
  
  constructor() { }

  ngOnInit(): void {  }

  public allRoles=['Developer','Manager','Admin'];
  selectedrole:string = "";

}
