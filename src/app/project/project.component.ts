import { Component, Input, OnInit, TemplateRef } from '@angular/core';
import { Project } from '../services/project/Project';
import { IconDefinition, faBriefcase, faPencil, faClock, faCalendarPlus } from '@fortawesome/free-solid-svg-icons';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AddTimeDayComponent } from '../add-time-day/add-time-day.component';
import { User } from '../services/User';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {

  @Input() project: Project | undefined;

  faBriefcase: IconDefinition = faBriefcase;
  faClock: IconDefinition = faClock;
  faCalendarPlus: IconDefinition = faCalendarPlus;

  constructor(private dialog: MatDialog) { }

  ngOnInit(): void {
  }

  addTimeDay(): void {
    console.log("Add TimeDay for id = " + this.project?.id)
  }

  openDialog(): void {
    let dialogRef = this.dialog.open(AddTimeDayComponent, {
      width: '100%',
      height: '100%',
      data: { project: this.project, user: 1 }
    });

    /*dialogRef.afterClosed().subscribe(result => {
      console.log("TimeDay sent!" + result);
    });*/
  }
}
