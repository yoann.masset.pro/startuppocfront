import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { GlobalVar } from '../global-variables';
import { Project } from '../services/project/Project';
import { ProjectService } from '../services/project/project.service';
import { IconDefinition, faPlusCircle } from '@fortawesome/free-solid-svg-icons';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-project-panel',
  templateUrl: './project-panel.component.html',
  styleUrls: ['./project-panel.component.css']
})
export class ProjectPanelComponent implements OnInit {

  faPlusCircle: IconDefinition = faPlusCircle;

  projects: Observable<Project[]> | any;
  constructor(private projectService: ProjectService, private _router: Router) {
    if (!GlobalVar.connectedUser) {
      this._router.navigate(['login'])
    }
  }

  ngOnInit(): void {
    this.loadProject();
  }

  loadProject() {
    this.projects = this.projectService.load();
  }

  async addProject() {
    await Swal.fire({
      title: "Nom du nouveau projet",
      input: "text",
      icon: "question",
      inputPlaceholder: "Nom du projet",
      confirmButtonText: "Créer",
      confirmButtonColor: "green",
      showCancelButton: true,
      cancelButtonText: "Annuler",
    }).then(async result => {
      if (result.isConfirmed) {
        console.log(result.value);
        this.projectService.post(result.value);
        Swal.showLoading();
        await Swal.fire("Nouveau projet créé !", result.value, "success");
        this.loadProject();
      }
    });
  }
}
