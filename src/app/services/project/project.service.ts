import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Project } from './Project';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  api: string = "http://localhost:8081/project"

  constructor(private http: HttpClient) { }

  load() {
    return this.http.get<Project[]>(this.api + 's');
  }

  post(name: string) {
    const headers = { 'content-type': 'application/json' };
    const project = {
      name: name,
    };
    const body = JSON.stringify(project);
    console.log(body);
    this.http.post(this.api, body, { 'headers': headers }).subscribe(data => { console.log(data); return data });
  }
}
