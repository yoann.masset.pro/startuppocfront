import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { GlobalVar } from '../global-variables';
import { User } from '../services/User';
import { UsersService } from '../services/users.service';

@Component({
  selector: 'app-manager-panel',
  templateUrl: './manager-panel.component.html',
  styleUrls: ['./manager-panel.component.css']
})
export class ManagerPanelComponent implements OnInit {

  @Input() user:User|null=null;

  @Output() childEvent = new EventEmitter();
  switchRole(){
    this.managers.forEach((value,index) => {
      if(value.login == this.selectedmanager){
        this.newManager=value;
      }
    });
    this.childEvent.emit(this.newManager);
  }
  
  constructor(private usersService : UsersService) { }

  ngOnInit(): void {
    this.usersService.loadUsers().subscribe(data => {
      data.forEach((value,index) => {
        if(value.type.id != 1){
          
          this.managers.push(value);
        }
      });
      
      console.log(this.managers)
    });
  }

  public managers:User[]=[];
  selectedmanager:string = "";
  newManager:User|null=null;
}
