import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import * as CryptoJS from 'crypto-js';
import { SigninService } from '../services/signin.service';
import { User } from '../services/User';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { GlobalVar } from '../global-variables';

@Component({
  selector: 'app-signin-form',
  templateUrl: './signin-form.component.html',
  styleUrls: ['./signin-form.component.css']
})
export class SigninFormComponent implements OnInit {

  constructor(private singinService: SigninService) { }

  ngOnInit(): void {
  }

  @Input() roles: any[] | undefined;

  @Output() childEvent = new EventEmitter();
  addUser() {
    this.childEvent.emit(this.user);
  }

  faUser = faUser;

  login: string = "";
  password: string = "";
  firstname: string = "";
  lastname: string = "";
  selectedrole: string = "";
  role: number = 0;
  encryptedPassword: string = "";
  user: User = { id: 0, login: "", password: "", firstname: "", lastname: "", type: { id: 0, name: "" } }
  isloginvalid: boolean = true;
  ispasswordvalid: boolean = true;
  isfirstnamevalid: boolean = true;
  islastnamevalid: boolean = true;
  isRoleValid: boolean = true;
  invalidfield: boolean = false;
  allowedCharRegex: RegExp = new RegExp('^[a-zA-Z0-9@._]+$');

  setUserRole() {
    if (this.selectedrole == "Developer") { this.role = 1 }
    if (this.selectedrole == "Manager") { this.role = 2 }
  }

  checkFieldValidity() {

    if (typeof this.password != 'undefined' && this.password && this.allowedCharRegex.test(this.password)) {
      this.ispasswordvalid = true;
    } else {
      this.ispasswordvalid = false;
      console.log("invalid password");
    }

    if (typeof this.login != 'undefined' && this.login && this.allowedCharRegex.test(this.login)) {
      this.isloginvalid = true;
    } else {
      this.isloginvalid = false;
      console.log("invalid login");
    }

    if (typeof this.firstname != 'undefined' && this.firstname && this.allowedCharRegex.test(this.firstname)) {
      this.isfirstnamevalid = true;
    } else {
      this.isfirstnamevalid = false;
      console.log("invalid firstname");
    }

    if (typeof this.lastname != 'undefined' && this.lastname && this.allowedCharRegex.test(this.lastname)) {
      this.islastnamevalid = true;
    } else {
      this.islastnamevalid = false;
      console.log("invalid lastname");
    }

    if (typeof this.role != 'undefined' && this.role && this.role > 0) {
      this.isRoleValid = true;
    } else {
      this.isRoleValid = false;
      console.log("invalid role");
    }

    if (this.isloginvalid && this.ispasswordvalid && this.isfirstnamevalid && this.islastnamevalid && this.isRoleValid) {
      this.invalidfield = false;
      return true;
    } else {
      this.invalidfield = true;
      return false
    }

  }

  Signin(): void {
    this.setUserRole();
    if (this.checkFieldValidity()) {
      this.encryptPassword();

      this.user = {
        id: 0,
        firstname: this.firstname,
        lastname: this.lastname,
        login: this.login,
        password: this.encryptedPassword,
        type: { id: this.role, name: this.selectedrole }
      }

      this.addUser();
    }
  }


  encryptPassword() {
    var hash = CryptoJS.HmacSHA256(this.password, "mysecretkey");
    this.encryptedPassword = CryptoJS.enc.Base64.stringify(hash);
  }


}
