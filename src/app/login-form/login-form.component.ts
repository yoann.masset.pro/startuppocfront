import { Component, OnInit } from '@angular/core';
import * as CryptoJS from 'crypto-js';
import { LoginForm } from '../services/LoginForm';
import { LoginService } from '../services/login.service';
import { Router } from '@angular/router';
import { GlobalVar } from '../global-variables';
import { User } from '../services/User'
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  login: string = "";
  password: string = "";
  encryptedPassword: string = "";
  loginform: LoginForm = { login: "", password: "" }
  isloginvalid: boolean = true;
  ispasswordvalid: boolean = true;
  invalidfield: boolean = false;
  allowedCharRegex: RegExp = new RegExp('^[a-zA-Z0-9@._]+$');


  constructor(private loginService: LoginService, private _router: Router) { }

  ngOnInit(): void {
  }

  checkFieldValidity() {

    if (typeof this.password != 'undefined' && this.password && this.allowedCharRegex.test(this.password)) {
      this.ispasswordvalid = true;
    } else {
      this.ispasswordvalid = false;
      console.log("invalid password");
    }

    if (typeof this.login != 'undefined' && this.login && this.allowedCharRegex.test(this.login)) {
      this.isloginvalid = true;
    } else {
      this.isloginvalid = false;
      console.log("invalid login");
    }

    if (this.isloginvalid && this.ispasswordvalid) {
      this.invalidfield = false;
      return true;
    } else {
      this.invalidfield = true;
      return false
    }

  }

  async Login(): Promise<void> {
    if (this.checkFieldValidity()) {
      this.encryptPassword();

      this.loginform = {
        login: this.login,
        password: this.encryptedPassword,
      }
      this.loginService.LogIn(this.loginform).subscribe((data) => {

        if (data) {
          GlobalVar.connectedUser = data;
          this._router.navigate(['home'])
          console.log('successfully connected with user ', GlobalVar.connectedUser?.login)
          const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
              toast.addEventListener('mouseenter', Swal.stopTimer)
              toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
          });

          Toast.fire({
            icon: 'success',
            title: 'Connexion réussite'
          });
        } else {
          console.log('could not connect with this user ')
        }
      }

      );


    }

  }

  encryptPassword() {
    var hash = CryptoJS.HmacSHA256(this.password, "mysecretkey");
    this.encryptedPassword = CryptoJS.enc.Base64.stringify(hash);
  }

}
