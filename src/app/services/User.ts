export interface User {
    id: number;
    firstname: string;
    lastname: string;
    login: string;
    password: string;
    type: UserType;
}

export interface UserType {
    id: number,
    name: string,
}