import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TimeDayComponent } from './time-day.component';

describe('TimeDayComponent', () => {
  let component: TimeDayComponent;
  let fixture: ComponentFixture<TimeDayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TimeDayComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TimeDayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
