import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationBarComponent } from './navigation-bar/navigation-bar.component';
import { FooterComponent } from './footer/footer.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { SigninFormComponent } from './signin-form/signin-form.component';
import { HttpClientModule } from '@angular/common/http';
import { UserpanelComponent } from './userpanel/userpanel.component';
import { MatDialogModule, MatDialogActions, MatDialogClose, MatDialogContent } from '@angular/material/dialog';
import { ProjectPanelComponent } from './project-panel/project-panel.component';
import { ProjectComponent } from './project/project.component';
import { AddTimeDayComponent } from './add-time-day/add-time-day.component';
import { FullCalendarModule } from '@fullcalendar/angular';
import { TimeDayComponent } from './time-day/time-day.component';
import { HomeComponent } from './home/home.component';
import { RolePanelComponent } from './role-panel/role-panel.component';
import { ManagerPanelComponent } from './manager-panel/manager-panel.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationBarComponent,
    FooterComponent,
    LoginFormComponent,
    SigninFormComponent,
    UserpanelComponent,
    ProjectPanelComponent,
    ProjectComponent,
    AddTimeDayComponent,
    TimeDayComponent,
    HomeComponent,
    RolePanelComponent,
    ManagerPanelComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    FontAwesomeModule,
    HttpClientModule,
    MatDialogModule,
    FullCalendarModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
