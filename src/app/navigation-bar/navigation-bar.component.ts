import { Component, OnInit } from '@angular/core';
import { faUser, faSignIn,faCalendar, faHouse, faBars, faUsersGear, faDiagramProject, faClock } from '@fortawesome/free-solid-svg-icons';
import { GlobalVar } from '../global-variables';

@Component({
  selector: 'app-navigation-bar',
  templateUrl: './navigation-bar.component.html',
  styleUrls: ['./navigation-bar.component.css']
})
export class NavigationBarComponent implements OnInit {

  faUser = faUser;
  faSignIn = faSignIn;
  faHouse = faHouse;
  faBars = faBars;
  faUsersGear = faUsersGear;
  faDiagramProject = faDiagramProject;
  faClock = faClock;
  faCalendar = faCalendar;

  globalVar = GlobalVar;

  usersHover: boolean = false;
  projectsHover: boolean = false;
  homeHover: boolean = false;
  loginHover: boolean = false;
  timedaysHover: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

}
