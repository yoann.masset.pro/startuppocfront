import { Component, OnInit, TemplateRef } from '@angular/core';
import { faUser, faEye, faUserPlus,faTimes,faCalendar, faEllipsisH, faTrash, faUserAstronaut, faUserEdit } from '@fortawesome/free-solid-svg-icons';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { User, UserType } from '../services/User';
import { UsersService } from '../services/users.service';
import { SigninService } from '../services/signin.service'
import { GlobalVar } from '../global-variables';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-userpanel',
  templateUrl: './userpanel.component.html',
  styleUrls: ['./userpanel.component.css']
})
export class UserpanelComponent implements OnInit {

  constructor(private dialog: MatDialog, private usersService: UsersService, private signinService: SigninService, private _router: Router) {
    if (!GlobalVar.connectedUser) {
      this._router.navigate(['login'])
    }
  }

  ngOnInit() {
    if (GlobalVar.connectedUser?.type.id == 3) this.usersService.loadUsers().subscribe(data => this.us = data);
    else this.usersService.loadDevelopers(this.connectedUser?.id ?? 0).subscribe(data => this.us = data);
  }

  public allRoles = ['Developer', 'Manager', 'Admin'];
  selectedrole: string = "";
  switchingRole: UserType | null = null;
  switchingUser: User | null = null;
  public roles = GlobalVar.connectedUser?.type!.id! > 2 ? ['Developer', 'Manager'] : ['Developer'];
  connectedUser = GlobalVar.connectedUser;
  us: User[] = [];


  faUser = faUser;
  faUserPlus = faUserPlus;
  faTimes=faTimes;
  faEye=faEye;
  faEllipsisH=faEllipsisH;
  faTrash=faTrash;
  faUserAstronaut=faUserAstronaut;
  faUserEdit=faUserEdit;
  faCalendar=faCalendar;

  globalVar = GlobalVar;

  setUserRole() {
    console.log(this.selectedrole);
    if (this.selectedrole == "Developer") { this.switchingRole = { id: 1, name: "Developer" } }
    if (this.selectedrole == "Manager") { this.switchingRole = { id: 1, name: "Manager" } }
    if (this.selectedrole == "Admin") { this.switchingRole = { id: 1, name: "Admin" } }
  }


  openDialog(templateRef: TemplateRef<any>) {
    let dc = new MatDialogConfig();
    dc.panelClass = "dialog";
    dc.width = "50%";
    this.dialog.open(templateRef, dc);
  }

  addUser(userToAdd: any) {
    console.log(userToAdd)
    this.dialog.closeAll();
    this.signinService.SignIn(userToAdd).subscribe(userAdded => {
      console.log(userAdded);
      this.usersService.addDeveloper(this.connectedUser?.id ?? 0, userAdded).subscribe(() => {
        if (GlobalVar.connectedUser?.type.id == 3) this.usersService.loadUsers().subscribe(data => this.us = data);
        else this.usersService.loadDevelopers(this.connectedUser?.id ?? 0).subscribe(data => this.us = data);
      });
    });

    //window.location.reload();   this.connectedUser?.id??0
  }

  async removeUser(id: number) {
    await Swal.fire({
      title: "Voulez vous supprimer cet utilisateur ?",
      text: "Supprimer un utilisateur est irreversible !",
      icon: "question",
      confirmButtonText: "Oui, Supprimer !",
      confirmButtonColor: 'red',
      showCancelButton: true,
      cancelButtonText: "Non",
    }).then(async (result) => {
      if (result.isConfirmed) {
        this.usersService.deleteData(id).subscribe(res => {
          if (GlobalVar.connectedUser?.type.id == 3) this.usersService.loadUsers().subscribe(data => this.us = data);
          else this.usersService.loadDevelopers(this.connectedUser?.id ?? 0).subscribe(data => this.us = data);
        });
        Swal.showLoading();
        await Swal.fire("Utilisateur supprimé !", "", "success");
      }
    });
  }

changeDialog(user:User,ref:any){
  this.switchingUser= user;
  console.log('opening dialog to operate changes on  ',this.switchingUser?.login);
  this.openDialog(ref);
}

switchRole(selectedRole: any){
  this.selectedrole=selectedRole;
  this.setUserRole();
  console.log(this.switchingRole)
  if(this.switchingUser && this.switchingRole){
    console.log('switching ', this.switchingUser.login, "'s role to ",this.switchingRole,' ...' );
    
    this.usersService.switchRole(this.switchingUser,this.switchingRole).subscribe(data => {
      this.dialog.closeAll();
      if (GlobalVar.connectedUser?.type.id == 3) this.usersService.loadUsers().subscribe(data => this.us = data);
      else this.usersService.loadDevelopers(this.connectedUser?.id??0).subscribe(data => this.us = data);
    });
  }else{
    console.log('error when switching role');
  }
}

switchManager(selectedManager: any){
  if(this.switchingUser && selectedManager && this.connectedUser){
    console.log('switching ', this.switchingUser.login, "'s manager to ",selectedManager,' ...' );
    
    this.usersService.switchManager(this.switchingUser,this.connectedUser,selectedManager).subscribe(data => {
      this.dialog.closeAll();
      if (GlobalVar.connectedUser?.type.id == 3) this.usersService.loadUsers().subscribe(data => this.us = data);
      else this.usersService.loadDevelopers(this.connectedUser?.id??0).subscribe(data => this.us = data);
    });
  }else{
    console.log('error when switching role');
  }
}

showAgenda(targetUser:User,ref:any){
  this.switchingUser= targetUser;
  console.log('opening dialog to show agenda');
  this.openDialog(ref);
}
  
}
