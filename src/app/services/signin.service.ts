import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from './User';

@Injectable({
  providedIn: 'root'
})
export class SigninService {

  apiUrl: string = 'http://localhost:8081/user'

  constructor(private http: HttpClient) { }

  SignIn(user: User) {
    const headers = { 'content-type': 'application/json' }
    const body = JSON.stringify(user);
    return this.http.post<any>(this.apiUrl, body, { 'headers': headers });
  }
}
