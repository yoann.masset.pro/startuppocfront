import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalVar } from '../global-variables';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  connectedUser = GlobalVar.connectedUser;
  ROLE = ["NULL", "Developpeur", "Manager", "Administrateur"];

  constructor(private _router: Router) {
    if (!GlobalVar.connectedUser) {
      this._router.navigate(['login'])
    }
  }

  ngOnInit(): void {
  }

}
