import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AddTimeDayComponent } from './add-time-day/add-time-day.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { ProjectPanelComponent } from './project-panel/project-panel.component';
import { SigninFormComponent } from './signin-form/signin-form.component';
import { TimeDayComponent } from './time-day/time-day.component';
import { UserpanelComponent } from './userpanel/userpanel.component';
import { RolePanelComponent } from './role-panel/role-panel.component';
import { ManagerPanelComponent } from './manager-panel/manager-panel.component';

const routes: Routes = [
  {path: 'login', component: LoginFormComponent},
  {path: 'signin', component: SigninFormComponent},
  {path: 'userpanel', component:UserpanelComponent},
  {path: 'projectpanel', component: ProjectPanelComponent},
  {path: 'agenda', component: TimeDayComponent},
  {path: 'home', component:HomeComponent},
  {path: '', redirectTo: 'login', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
